from extractor.MetricTreeNode import MetricTreeNode

__author__ = 'michal'


def __print_metric_tree(metric_tree_node, indent):
    if type(metric_tree_node) is MetricTreeNode:
        print " " * indent + "(" + str(metric_tree_node.energy) + ")"
        for child in metric_tree_node.children:
            __print_metric_tree(child, indent + 1)
    else:
        print " " * indent + "(" + str(metric_tree_node.energy) + ")", metric_tree_node.text


def print_metric_tree(metric_tree):
    __print_metric_tree(metric_tree, 0)
