# coding=utf-8
import codecs
from extractor.Extractor import Extractor
from extractor.metric.apartmentinfo.ApartmentInfoMetric import \
    ApartmentInfoMetric

__author__ = 'michal'


class EffectivenessTestCaseOutput(object):
    def __init__(
            self,
            number_of_successful_comparisons,
            total_number_of_comparisons,
            messages
    ):
        super(EffectivenessTestCaseOutput, self).__init__()
        self.number_of_successful_comparisons = number_of_successful_comparisons
        self.total_number_of_comparisons = total_number_of_comparisons
        self.messages = messages


class EffectivenessTestCase(object):
    def __init__(self, file_path, expected_data_dict):
        super(EffectivenessTestCase, self).__init__()
        self.file_path = file_path
        self.expected_data = expected_data_dict

    def test(self):
        extractor = Extractor(ApartmentInfoMetric())
        with codecs.open(self.file_path, encoding="utf-8") as content_file:
            content = content_file.read()
        actual_data = extractor.extract(content)
        total_number = 0
        successful_number = 0
        messages = []
        for property_key in self.expected_data:
            if self.expected_data[property_key] is None:
                continue
            total_number += 1
            if property_key in actual_data and \
                            actual_data[property_key] == self.expected_data[
                        property_key]:
                successful_number += 1
            else:
                messages.append(self._create_incorrect_data_message(actual_data,
                                                                    property_key))
        return EffectivenessTestCaseOutput(successful_number, total_number,
                                           messages)

    def _create_incorrect_data_message(self, actual_data, property_key):
        if property_key in actual_data:
            actual_value = actual_data[property_key]
        else:
            actual_value = None
        return u"Incorrect {0}, expected: {1}, actual: {2}".format(
            property_key,
            self.expected_data[property_key],
            actual_value
        )


class EffectivenessTestSuite(object):
    def __init__(self, test_cases):
        super(EffectivenessTestSuite, self).__init__()
        self.test_cases = test_cases

    def run(self):
        successful_comparisons = 0
        total_comparisons = 0
        failed_tests = 0
        for test_case in self.test_cases:
            assert isinstance(test_case, EffectivenessTestCase)
            output = test_case.test()
            successful_comparisons += output.number_of_successful_comparisons
            total_comparisons += output.total_number_of_comparisons
            if output.number_of_successful_comparisons < output \
                    .total_number_of_comparisons:
                failed_tests += 1
                print ""
                print "*** Error ***"
                print "*** Data extracted from the following file is " \
                      "incorrect:"
                print test_case.file_path
                if output.messages:
                    print "*** Messages:"
                    for message in output.messages:
                        print message
        print ""
        print "Effectivity: %d/%d (%.1f %%)" % (
            successful_comparisons, total_comparisons,
            100.0 * successful_comparisons / total_comparisons
        )