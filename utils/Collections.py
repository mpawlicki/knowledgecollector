__author__ = 'michal'


class ThreadUnsafeQueue(object):
    class Node(object):
        def __init__(self, value, next_node):
            self.value = value
            self.next = next_node

    def __init__(self):
        self.head = None
        self.tail = None

    def add(self, value):
        node = ThreadUnsafeQueue.Node(value, None)
        if self.tail is None:
            self.tail = self.head = node
        else:
            self.tail.next = node
            self.tail = node

    def remove(self):
        if self.head is None:
            raise Exception()
        if self.head == self.tail:
            self.tail = None
        value = self.head.value
        self.head = self.head.next
        return value

    def is_empty(self):
        return self.head is None

    def peek(self):
        if self.head is None:
            raise Exception()
        return self.head.value

