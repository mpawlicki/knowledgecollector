import codecs

from bs4 import BeautifulSoup
# from crawler.crawl import WebsiteDownloader
from extractor.BeautifulSoupUtils import strip_tags, merge_tag_contents
from extractor.CoherentNodeFinder import find_coherent_nodes
from extractor.CoherentNodeToObjectConverter import \
    CoherentNodeToObjectConverter
from extractor.HtmlToMetricTreeConverter import HtmlToMetricTreeConverter
from extractor.metric.apartmentinfo.ApartmentInfoMetric import \
    ApartmentInfoMetric
from extractor.preprocessing import replace_nbsps, replace_twos_in_superscripts, \
    clean_white_characters
from utils.MetricTreePrinter import print_metric_tree


__author__ = 'michal'

filename = "testpages/gumtree1.html"
with codecs.open(filename, encoding="utf-8") as content_file:
    content = content_file.read()
content = replace_twos_in_superscripts(
    replace_nbsps(
        content))
soup = BeautifulSoup(content)
strip_tags(soup, ["a", "b", "i", "span", "strong", "sup", "u"])
soup = BeautifulSoup(clean_white_characters(soup.prettify()))
print soup.prettify()
# url = "http://otodom.pl/mieszkanie-warszawa-ochota-27,
# 40m2-1-pokoj-267000-pln-id27375393.html"
# print "Downloading: " + url
# content = WebsiteDownloader().download(url)
# print "Download finished."
# soup = BeautifulSoup(content)
# soup.decode(eventual_encoding="utf-8")
metric = ApartmentInfoMetric()
print "Conversion started"
metric_tree = HtmlToMetricTreeConverter(20.0).convert(soup.body, metric)
print "Conversion finished"
# print_metric_tree(metric_tree)
coherent_nodes = find_coherent_nodes(metric_tree)
apartment_info = CoherentNodeToObjectConverter().convert(coherent_nodes)
# print "Address:", apartment_info.address
# print "Surface:", apartment_info.surface
#print "Price:", apartment_info.price
#print "Phone:", apartment_info.phone_number
#print "Email:", apartment_info.email
#for node in coherent_nodes:
#    print_metric_tree(node)
print apartment_info
