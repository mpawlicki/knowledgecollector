import gtk
import re

from bs4 import BeautifulSoup
from ghost import Ghost, TimeoutError


__author__ = 'michal'


class WebsiteDownloader(object):
    """
    Fetches HTML of single website,
    invokes JavaScript to achieve final markup
    """

    def __init__(self):
        super(WebsiteDownloader, self).__init__()
        self.ghost = Ghost()

    def download(self, url):
        try:
            page, extra_resources = self.ghost.open(url)
            return page.content
        except TimeoutError:
            raise


class Crawler(object):
    """
    Crawls pages whose addresses match the specified pattern, starting from
    the specified URL and following the links found.
    """
    def __init__(self, start_url, pattern=r'.+'):
        """
        :param pattern: a regular expression string specifying which links
         to follow
        :param start_url:
        """
        super(Crawler, self).__init__()
        self._downloader = WebsiteDownloader()
        self._start_url = start_url
        self._regex = re.compile(pattern)
        gtk.gdk.threads_init()

    def crawl(self):
        awaiting_urls = {self._start_url}
        while awaiting_urls:
            self._process_next_url(awaiting_urls)

    def _process_next_url(self, awaiting_urls):
        current_url = awaiting_urls.pop()
        page_content = self._try_to_download_page(current_url)
        if page_content:
            self.on_download(current_url, page_content)
            urls_to_follow = self._find_urls_to_follow(page_content,
                                                       self._regex)
            awaiting_urls.update(urls_to_follow)

    def _try_to_download_page(self, current_url):
        try:
            return self._downloader.download(current_url)
        except TimeoutError:
            return None

    # noinspection PyMethodMayBeStatic
    def on_download(self, url, page_content):
        """
        Invoked each time a page is downloaded. Allows performing some kind
          of action on the page and its url. Override this method to specify
          the desired behavior.
        """
        pass

    @staticmethod
    def _find_urls_to_follow(page_content, url_regex):
        soup = BeautifulSoup(page_content)
        links = soup.find_all('a')
        urls = map(lambda link: link.get('href'), links)
        unique_urls = set(urls)
        matching_urls = filter(lambda url: url and url_regex.match(url),
                               unique_urls)
        return matching_urls


def crawl():
    class PrintingCrawler(Crawler):
        def on_download(self, url, page_content):
            super(PrintingCrawler, self).on_download(url, page_content)
            print url

    crawler = PrintingCrawler(
        'http://www.gumtree.pl/fp-mieszkania-i-domy-sprzedam-i-kupie/c9073',
        r'^(http://)?www\.gumtree\.pl/'
        r'[cf]p-mieszkania-i-domy-sprzedam-i-kupie/.+$'
    )
    crawler.crawl()


if __name__ == '__main__':
    crawl()