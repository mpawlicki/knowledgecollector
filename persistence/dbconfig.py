# coding=utf-8

# Database server connection
DB_HOST = None  # defaults to 127.0.0.1
DB_PORT = None  # defaults to 5984

# Database settings
DB_NAME = 'apartment_infos'
