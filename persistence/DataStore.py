from uuid import uuid4

import couchdb

from dbconfig import *


class DataStore(object):
    def __init__(self, database_host=DB_HOST, database_port=DB_PORT, database_name=DB_NAME):
        assert database_name, 'Database name not provided'

        server_url = database_host + database_port if database_host and database_port else None
        server = couchdb.Server(server_url) if server_url else couchdb.Server()

        self.__database = server[database_name] if database_name in server else server.create(database_name)
        self.__server = server

    def save(self, document):
        if '_id' not in document:
            document_id = uuid4().hex
            document['_id'] = document_id

        self.__database.save(document)

        return document['_id']

    def save_all(self, docs):
        return self.__database.update(docs)

    def __repr__(self):
        return self.__database.resource.url

    def clear(self):
        database_name = self.__database.name
        self.__server.delete(database_name)
        self.__database = self.__server.create(database_name)