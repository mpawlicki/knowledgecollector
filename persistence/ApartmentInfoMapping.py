from couchdb.mapping import *


class ApartmentInfoMapping(Document):
    address = TextField()
    surface = DecimalField()
    price = IntegerField()
    phone_number = TextField()
    email = TextField()
    number_of_rooms = IntegerField()
    submission_date = DateField()