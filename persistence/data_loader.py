from persistence.ApartmentInfoMapping import ApartmentInfoMapping
from persistence.DataStore import DataStore
from persistence.dbconfig import *
from testsuite import test_cases


def load_data():
    print 'Connecting to database...'
    print 'HOST:', DB_HOST or '127.0.0.1'
    print 'PORT:', DB_PORT or '5984'
    print 'DATABASE:', DB_NAME
    data_store = DataStore()

    print 'Emptying database...'
    data_store.clear()

    print 'Inserting test data into database...'
    apartment_infos = [ApartmentInfoMapping(**test_case.expected_data) for test_case in test_cases]
    data_store.save_all(apartment_infos)

if __name__ == '__main__':
    load_data()
else:
    print 'No action. Please run this script from __main__ context.'