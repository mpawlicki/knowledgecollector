# coding=utf-8

from datetime import datetime

from extractor.apartmentinfo import ADDRESS_KEY, SURFACE_KEY, PRICE_KEY, \
    PHONE_NUMBER_KEY, EMAIL_KEY, NUMBER_OF_ROOMS_KEY, SUBMISSION_DATE_KEY
from utils.effectivenesstesting import EffectivenessTestCase, \
    EffectivenessTestSuite


__author__ = 'michal'

working_dir = "testpages/"
test_cases = [
    EffectivenessTestCase(working_dir + "gratka1.html", {
        ADDRESS_KEY: None,
        SURFACE_KEY: 80.50,
        PRICE_KEY: 399700,
        PHONE_NUMBER_KEY: "508106830",
        EMAIL_KEY: "info@villa.poznan.pl",
        NUMBER_OF_ROOMS_KEY: 4,
        SUBMISSION_DATE_KEY: datetime(2013, 3, 7).date()
    }),
    EffectivenessTestCase(working_dir + "gratka2.html", {
        ADDRESS_KEY: u"Kraków Krowodrza, ul. Wiarusa",
        SURFACE_KEY: 61,
        PRICE_KEY: 470000,
        PHONE_NUMBER_KEY: "501429976",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 2,
        SUBMISSION_DATE_KEY: datetime(2014, 5, 3).date()
    }),
    EffectivenessTestCase(working_dir + "gratka3.html", {
        ADDRESS_KEY: u"Kraków Prądnik Czerwony, Śródmieście, ul. Powstańców",
        SURFACE_KEY: 45,
        PRICE_KEY: 259000,
        PHONE_NUMBER_KEY: "507070540",
        EMAIL_KEY: "BIURO@MAYA.NIERUCHOMOSCI.PL",
        NUMBER_OF_ROOMS_KEY: 3,
        SUBMISSION_DATE_KEY: datetime(2014, 5, 31).date()
    }),
    EffectivenessTestCase(working_dir + "gumtree1.html", {
        ADDRESS_KEY: "Stare Babice, Polska",
        SURFACE_KEY: 160,
        PRICE_KEY: 799000,
        PHONE_NUMBER_KEY: "600035168",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 5,
        SUBMISSION_DATE_KEY: datetime(2014, 1, 31).date()
    }),
    EffectivenessTestCase(working_dir + "gumtree2.html", {
        ADDRESS_KEY: u"Powiśle, Warszawa, Polska",
        SURFACE_KEY: 47,
        PRICE_KEY: 420000,
        PHONE_NUMBER_KEY: "723700006",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 3,
        SUBMISSION_DATE_KEY: datetime(2014, 2, 26).date()
    }),
    EffectivenessTestCase(working_dir + "gumtree3.html", {
        ADDRESS_KEY: u"Skrajna, Warszawa, Polska",
        SURFACE_KEY: 38,
        PRICE_KEY: 229000,
        PHONE_NUMBER_KEY: "501087525",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 2,
        SUBMISSION_DATE_KEY: datetime(2014, 2, 27).date()
    }),
    EffectivenessTestCase(working_dir + "gumtree4.html", {
        ADDRESS_KEY: u"Tatrzańska, Łódź, Polska",
        SURFACE_KEY: 28,
        PRICE_KEY: 110000,
        PHONE_NUMBER_KEY: None,
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: None,
        SUBMISSION_DATE_KEY: datetime(2014, 3, 1).date()
    }),
    EffectivenessTestCase(working_dir + "otodom1.html", {
        ADDRESS_KEY: u"Kołobrzeg, Nadmorska, Sułkowskiego, pow. Kołobrzeski",
        SURFACE_KEY: 49,
        PRICE_KEY: 570000,
        PHONE_NUMBER_KEY: "668027822",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 2,
        SUBMISSION_DATE_KEY: datetime(2014, 3, 1).date()
    }),
    EffectivenessTestCase(working_dir + "otodom2.html", {
        ADDRESS_KEY: u"Kraków, Krowodrza, ul. Wybickiego",
        SURFACE_KEY: 31.50,
        PRICE_KEY: 251591,
        PHONE_NUMBER_KEY: "501521408",
        EMAIL_KEY: None,
        NUMBER_OF_ROOMS_KEY: 1,
        SUBMISSION_DATE_KEY: datetime(2014, 5, 4).date()
    })
]
test_suite = EffectivenessTestSuite(test_cases)


def main():
    test_suite.run()


if __name__ == '__main__':
    main()