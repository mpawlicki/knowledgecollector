from extractor.normailzation.RegexReplacingNormalizer import RegexReplacingNormalizer
from extractor.normailzation.ReplacingNormalizer import ReplacingNormalizer
from extractor.normailzation.ChainingNormalizer import ChainingNormalizer

__separator = ""

_replacements = [("\\", __separator),
                 (" ", __separator),
                 ("(", __separator),
                 (")", __separator),
                 ("-", __separator)]

_regexReplacements = [(r"[+]\d{2,3}[^\d]", __separator)]


class PhoneNumberNormalizer(ChainingNormalizer):
    def __init__(self):
        super(PhoneNumberNormalizer, self).__init__(RegexReplacingNormalizer(_regexReplacements),
                                                    ReplacingNormalizer(_replacements))
