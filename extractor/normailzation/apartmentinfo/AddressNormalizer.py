class AddressNormalizer(object):
    def normalize(self, value):
        return value.strip() if value else None
