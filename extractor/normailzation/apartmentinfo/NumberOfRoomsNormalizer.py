from extractor.normailzation.NormalizationError import NormalizationError


class NumberOfRoomsNormalizer(object):
    def normalize(self, value):
        try:
            return int(value) if value else None
        except ValueError:
            raise NormalizationError
