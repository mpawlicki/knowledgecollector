from datetime import datetime
from extractor.normailzation.NormalizationError import NormalizationError
from extractor.normailzation.ReplacingNormalizer import ReplacingNormalizer

_separator = "."
_replacements = [(" ", _separator),
                 ("\\", _separator),
                 ("/", _separator),
                 (",", _separator),
                 ("-", _separator)]


class SubmissionDateNormalizer(ReplacingNormalizer):
    def __init__(self):
        super(SubmissionDateNormalizer, self).__init__(_replacements)

    def normalize(self, value):
        normalized = super(SubmissionDateNormalizer, self).normalize(value)

        if normalized:
            split = normalized.split(_separator)
            try:
                day = int(split[0])
                month = int(split[1])
                year = split[2] if len(split[2]) == 4 else "20" + split[2]
                year = int(year)
            except ValueError:
                raise NormalizationError

            normalized = datetime(year, month, day).date()

        return normalized