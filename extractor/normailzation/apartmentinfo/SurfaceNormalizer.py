from decimal import Decimal, InvalidOperation

from extractor.normailzation.ReplacingNormalizer import ReplacingNormalizer
from extractor.normailzation.NormalizationError import NormalizationError


_replacements = [(" ", ""),
                 (",", ".")]


class SurfaceNormalizer(ReplacingNormalizer):
    def __init__(self):
        super(SurfaceNormalizer, self).__init__(_replacements)

    def normalize(self, value):
        normalized = super(SurfaceNormalizer, self).normalize(value)
        try:
            return Decimal(normalized) if normalized else None
        except InvalidOperation:
            raise NormalizationError