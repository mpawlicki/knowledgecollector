from math import floor

from extractor.normailzation.ChainingNormalizer import ChainingNormalizer
from extractor.normailzation.ReplacingNormalizer import ReplacingNormalizer
from extractor.normailzation.NormalizationError import NormalizationError
from extractor.normailzation.RegexNormalizer import RegexNormalizer


_replacements = [(" ", ""),
                 (",", ".")]
_patterns = '([0-9]*[\.,]?[0-9]+)'


class PriceNormalizer(ChainingNormalizer):
    def __init__(self):

        super(PriceNormalizer, self).__init__(ReplacingNormalizer(_replacements), RegexNormalizer(_patterns))

    def normalize(self, value):
        normalized = super(PriceNormalizer, self).normalize(value)
        try:
            # any dot that is "in the middle" of price is dropped (eg. 123.456 -> 123456 but 1234.56 -> 1234.56)
            if normalized:
                normalized = ReplacingNormalizer([(".", "")]).normalize(normalized[:-3]) + normalized[-3:]

            return int(floor(float(normalized))) if normalized else None
        except ValueError:
            raise NormalizationError
