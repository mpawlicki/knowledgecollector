class ChainingNormalizer(object):
    def __init__(self, *args):
        self._normalizers = args

    def normalize(self, value):
        return reduce(lambda v, n: n.normalize(v), self._normalizers, value)
