class ReplacingNormalizer(object):
    def __init__(self, replacements):
        self._replacements = replacements

    def normalize(self, value):
        return reduce(lambda v, r: v.replace(r[0], r[1]), self._replacements, value) if value else None