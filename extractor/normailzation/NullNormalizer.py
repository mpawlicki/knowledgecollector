class NullNormalizer(object):
    """
    Null object providing no sophisticated normalization behavior
    """

    def normalize(self, value):
        return value
