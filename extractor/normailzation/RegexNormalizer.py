import re

from extractor.normailzation.NormalizationError import NormalizationError


class RegexNormalizer(object):
    def __init__(self, *patterns):
        self._patterns = [re.compile(pattern) for pattern in patterns]

    def normalize(self, value):
        def regex_normalize(v):
            for pattern in self._patterns:
                result = pattern.search(v)
                if result:
                    return result.group(1)

            raise NormalizationError

        return regex_normalize(value) if value else None