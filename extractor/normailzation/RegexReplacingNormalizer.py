import re


class RegexReplacingNormalizer(object):
    def __init__(self, regexReplacements):
        self._regexReplacements = [(re.compile(regexReplacement[0]), regexReplacement[1]) for regexReplacement in
                                   regexReplacements]

    def normalize(self, value):
        return reduce(lambda v, r: r[0].sub(r[1], v), self._regexReplacements, value) if value else None