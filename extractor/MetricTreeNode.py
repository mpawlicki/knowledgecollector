class MetricTreeNode(object):
    """
    Represents simple node in metric tree (but not a leaf)
    """

    def __init__(self, energy=0.0):
        self.children = []
        self.energy = energy

    def add_child(self, node):
        self.children.append(node)