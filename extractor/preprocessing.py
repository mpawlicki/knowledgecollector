import re

__author__ = 'michal'


NBSP = "&nbsp;"
TWO_IN_SUPERSCRIPT = "<sup>2</sup>"


def replace_nbsps(text):
    return text.replace(NBSP, " ")


def replace_twos_in_superscripts(text):
    return text.replace(TWO_IN_SUPERSCRIPT, "2")


def clean_white_characters(text):
    """
    :type text: str
    """
    lines = text.split("\n")
    stripped_lines = map(lambda line: line.strip(), lines)
    return reduce(lambda line1, line2: line1 + " " + line2, stripped_lines)