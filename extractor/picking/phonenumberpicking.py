# coding=utf-8
from extractor.picking import RegexBasedPicker, VALUE_GROUP_NAME, \
    surround_with_whitespaces

__author__ = 'michal'

_phone_number_keyword = ur"(telefon|tel\.?|kontakt|n(ume)?r tel(efonu|\.)?)"
__separator = ur"(\s+|-)"
_phone_number_value_1 = ur"(?P<{1}>(\+48{0}+)?\d{{3}}{0}\d{{3}}{0}\d{{3}})".format(
    __separator, VALUE_GROUP_NAME)
_phone_number_value_2 =\
    ur"(?P<{1}>(\+48{0}+)?\d{{2}}{0}\d{{2,3}}{0}\d{{2}}{0}\d{{2,3}})".format(
    __separator, VALUE_GROUP_NAME)

_full_regex_1 = _phone_number_keyword + ur"\s*" + _phone_number_value_1
_full_regex_2 = _phone_number_keyword + ur"\s*" + _phone_number_value_2

_value_regex_1 = surround_with_whitespaces(_phone_number_value_1)
_value_regex_2 = surround_with_whitespaces(_phone_number_value_2)


class PhoneNumberPicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [
            _full_regex_1,
            _full_regex_2,
            _value_regex_1,
            _value_regex_2
        ]
        keyword_regexes = [_phone_number_keyword]
        value_regexes = [_value_regex_1, _value_regex_2]
        super(PhoneNumberPicker, self).__init__(full_regexes, keyword_regexes,
                                                value_regexes)