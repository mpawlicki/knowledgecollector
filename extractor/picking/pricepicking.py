# coding=utf-8
from extractor.picking import VALUE_GROUP_NAME, RegexBasedPicker, \
    surround_with_whitespaces

__author__ = 'michal'

_price_keyword = ur"(cena)"
_currency = ur"(zł|zl|pln)"
_space = ur"(\s+)"
_price_value = ur"((\d{2,3}(" + _space + ur"|\.)+)\d{3}([,.]\d{2})?)"
_full_price_regex_1 = ur"{0}:?{1}?(?P<{2}>{3}){1}?{4}".format(
    _price_keyword, _space, VALUE_GROUP_NAME, _price_value, _currency)
_full_price_regex_2 = ur"{0}:? *{1}? *(?P<{3}>{2})".format(
    _price_keyword, _currency, _price_value, VALUE_GROUP_NAME)
_price_value_pattern = ur"(?P<{1}>{0})".format(_price_value, VALUE_GROUP_NAME)


class PricePicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [_full_price_regex_1, _full_price_regex_2]
        keyword_regexes = [_price_keyword]
        value_regexes = [surround_with_whitespaces(_price_value_pattern)]
        super(PricePicker, self).__init__(full_regexes, keyword_regexes,
                                          value_regexes)