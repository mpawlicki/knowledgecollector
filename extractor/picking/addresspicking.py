# coding=utf-8

from extractor.picking import RegexBasedPicker, VALUE_GROUP_NAME

__author__ = 'michal'

_address_keyword = ur"(adres:?|lokalizacja:?)"
_pre_word = ur"(ul\.|ulica|al\.|aleja|os\.|osiedle)"
_name = ur"(\w+(,?\s+\w+)*)"
_number = ur"(\d{1,3}(( *m\. *| */ *)\d{1,3}))"
_full_regex = ur"{0} *(?P<{4}>({1} *)?{2} *{3}?)".format(
    _address_keyword, _pre_word, _name, _number, VALUE_GROUP_NAME)
_only_name_regex = ur"(?P<{3}>({0} *)?{1} *{2}?)".format(_pre_word, _name,
                                                         _number,
                                                         VALUE_GROUP_NAME)


class AddressPicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [_full_regex]
        keyword_regexes = [_address_keyword]
        value_regexes = [_only_name_regex]
        super(AddressPicker, self).__init__(full_regexes, keyword_regexes,
                                            value_regexes)