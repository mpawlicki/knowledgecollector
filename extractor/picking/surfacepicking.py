# coding=utf-8

from extractor.picking import VALUE_GROUP_NAME, RegexBasedPicker, \
    surround_with_whitespaces

__author__ = 'michal'

_surface_keyword = ur"(powierzchnia|pow\.|wielko(ść|sc)|metra[zż])"
_square_meters = ur"(m *2|mkw|m\. *\kw.)"
_surface_value = ur"(\d{2,3}([,.]\d+)?)"
_full_surface_regex_1 = ur"{0} *(\(? *{1} *\)?)?:? *(?P<{3}>{2})".format(
    _surface_keyword, _square_meters, _surface_value, VALUE_GROUP_NAME)
_full_surface_regex_2 = ur"{0}:? *(?P<{3}>{1}) *(\(? *{2} *\)?)?".format(
    _surface_keyword, _surface_value, _square_meters, VALUE_GROUP_NAME)
_surface_with_square_meters_regex = ur"(?P<{2}>{0}) *\(? *{1} *\)?".format(
    _surface_value, _square_meters, VALUE_GROUP_NAME)
_surface_value_regex = ur"(?P<{0}>{1})".format(VALUE_GROUP_NAME,
                                               _surface_value)


class SurfacePicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [
            _full_surface_regex_1,
            _full_surface_regex_2,
            _surface_with_square_meters_regex
        ]
        keyword_regexes = [_surface_keyword]
        value_regexes = [surround_with_whitespaces(_surface_value_regex)]
        super(SurfacePicker, self).__init__(
            full_regexes,
            keyword_regexes,
            value_regexes)