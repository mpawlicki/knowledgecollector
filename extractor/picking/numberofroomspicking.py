# coding=utf-8
from extractor.picking import RegexBasedPicker, surround_with_whitespaces

__author__ = 'michal'

_separator = ur"(\s+|-)"
_number_keyword = ur"(liczba pokoi|pokoje)"
_room_keyword = ur"(pok[oó]j|pokoje|pokoi|pokojowe)"
_number_value = ur"(?P<value>\d{1,2})"

_full_regex_1 = _number_keyword + ur"(:?)" + _separator + ur"?" + _number_value
_full_regex_2 = _number_value + _separator + ur"?" + _room_keyword


class NumberOfRoomsPicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [_full_regex_1, _full_regex_2]
        keyword_regexes = [_number_keyword, _room_keyword]
        value_regexes = [surround_with_whitespaces(_number_value)]
        super(NumberOfRoomsPicker, self).__init__(full_regexes, keyword_regexes,
                                                  value_regexes)