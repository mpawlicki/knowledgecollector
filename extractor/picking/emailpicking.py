from extractor.picking import VALUE_GROUP_NAME, RegexBasedPicker

__author__ = 'michal'


_email_keyword = ur"((adres +)?e-?mail)"
_email_value = ur"(?P<{0}>[\w\.]+@(\w+\.)+\w+)".format(VALUE_GROUP_NAME)
_full_regex = _email_keyword + ur":? *" + _email_value


class EmailPicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [_full_regex, _email_value]
        keyword_regexes = [_email_keyword]
        value_regexes = [_email_value]
        super(EmailPicker, self).__init__(full_regexes, keyword_regexes,
                                          value_regexes)