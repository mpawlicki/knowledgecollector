from extractor.picking import RegexBasedPicker, surround_with_whitespaces

__author__ = 'michal'


_keyword = ur"(data dodania:?|dodano:?|zamieszczono:?)"
_space = ur"(\s+)"
_day = ur"([0-2]?[0-9]|3[01])"
_month = ur"(0?[0-9]|1[0-2])"
_year = ur"((20)?[0-9]{2})"
_separator = ur"(\.|/|-)"
_date_regex_1 = ur"(?P<value>{0}(?P<sep>{1}){2}(?P=sep){3})".format(
    _day, _separator, _month, _year
)
_date_regex_2 = ur"(?P<value>{0}(?P<sep>{1}){2}(?P=sep){3})".format(
    _year, _separator, _month, _day
)
_full_regex_1 = _keyword + _space + _date_regex_1
_full_regex_2 = _keyword + _space + _date_regex_2


class SubmissionDatePicker(RegexBasedPicker):
    def __init__(self):
        full_regexes = [_full_regex_1, _full_regex_2]
        keyword_regexes = [_keyword]
        value_regexes = map(
            surround_with_whitespaces,
            [_date_regex_1, _date_regex_2]
        )
        super(SubmissionDatePicker, self).__init__(full_regexes,
                                                   keyword_regexes,
                                                   value_regexes)