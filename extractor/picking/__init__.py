# coding=utf-8

__author__ = 'michal'

import re

RE_FLAGS = re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE
VALUE_GROUP_NAME = "value"


def surround_with_whitespaces(regex):
    """
    For a given regular expression returns an expression that matches only
    when it is surrounded with whitespaces or the beginning/end of a string.
    """
    return ur"(^|\s)" + regex + ur"($|\s)"


class DataPicker(object):
    """
    Interface for classes used during info extraction from a tree. A DataPicker
    tries to extract some data from the provided fragment of text.
    """

    def try_to_extract_info(self, text):
        """
        Tries to extract some data from the provided text. It can call the
        on_value_found function on the found value, remember some information
        for later extraction or do nothing.
        """
        return None


class RegexBasedPicker(DataPicker):
    """
    Extracts data from a given text using supplied regular expressions. Every
    regular expression must have a capture group named "value" that matches
    value of one type.

    When a full regex matches, the on_value_found function
    is called.

    When a keyword regex matches, the picker remembers that the next
    encountered value match should be treated as the right value.

    When a value regex matches, the picker treats that value as the right
    value if it encounters later a keyword regex match.
    """
    def __init__(self, full_regexes, keyword_regexes, value_regexes):
        super(RegexBasedPicker, self).__init__()

        compile_regex = lambda regex: re.compile(regex, RE_FLAGS)

        self._full_patterns = map(compile_regex, full_regexes)
        self._keyword_patterns = map(compile_regex, keyword_regexes)
        self._value_patterns = map(compile_regex, value_regexes)
        self._keyword_encountered = False
        self._value_encountered = None

    def try_to_extract_info(self, text):
        for full_pattern in self._full_patterns:
            potential_full_match = full_pattern.search(text)
            if potential_full_match:
                return potential_full_match.group(VALUE_GROUP_NAME)
        for keyword_pattern in self._keyword_patterns:
            potential_keyword_match = keyword_pattern.search(text)
            if potential_keyword_match:
                self._keyword_encountered = True
                return None
        for value_pattern in self._value_patterns:
            potential_value_match = value_pattern.search(text)
            if potential_value_match:
                found_value = potential_value_match.group(VALUE_GROUP_NAME)
                if self._keyword_encountered:
                    return found_value
                self._value_encountered = found_value
        return None