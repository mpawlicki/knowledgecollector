# coding=utf-8
from decimal import Decimal
from datetime import date

__author__ = 'michal'

ADDRESS_KEY = "address"
SURFACE_KEY = "surface"
PRICE_KEY = "price"
PHONE_NUMBER_KEY = "phone_number"
EMAIL_KEY = "email"
NUMBER_OF_ROOMS_KEY = "number_of_rooms"
SUBMISSION_DATE_KEY = "submission_date"

APARTMENT_INFO_KEYS = {
    ADDRESS_KEY,
    SURFACE_KEY,
    PRICE_KEY,
    PHONE_NUMBER_KEY,
    EMAIL_KEY,
    NUMBER_OF_ROOMS_KEY,
    SUBMISSION_DATE_KEY
}

REQUIRED_APARTMENT_INFO_TYPES = {
    ADDRESS_KEY: basestring,
    SURFACE_KEY: Decimal,
    PRICE_KEY: int,
    PHONE_NUMBER_KEY: basestring,
    EMAIL_KEY: basestring,
    NUMBER_OF_ROOMS_KEY: int,
    SUBMISSION_DATE_KEY: date
}


def has_complete_apartment_info(dictionary):
    """
    :type dictionary: dict
    """
    return all(map(lambda key: key in dictionary and dictionary[key],
                   APARTMENT_INFO_KEYS))


def has_only_raw_apartment_info(dictionary):
    """
    :type dictionary: dict
    """
    return all(map(
        lambda key: key not in APARTMENT_INFO_KEYS
            or key is None
            or isinstance(dictionary[key], basestring),
        dictionary))


def has_only_normalized_apartment_info(dictionary):
    """
    :type dictionary: dict
    """
    return all(map(
        lambda key: key not in APARTMENT_INFO_KEYS
            or key is None
            or isinstance(dictionary[key], REQUIRED_APARTMENT_INFO_TYPES[key]),
        dictionary))