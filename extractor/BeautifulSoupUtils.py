import bs4
from bs4.element import NavigableString


def is_node(soup):
    """
    = Is not a leaf in a Beautiful Soup tree
    """
    return 'childGenerator' in dir(soup)


def find_tree_depth(soup):
    def _find_tree_depth(soup, depth):
        depth += 1

        if not is_node(soup):
            return depth

        try:
            return max(_find_tree_depth(child_soup, depth) for child_soup in
                       soup.children)
        except ValueError:
            return depth

    return _find_tree_depth(soup, 0)


def strip_tags(soup, tags):
    """
    :type soup: bs4.BeautifulSoup
    """
    for tag in soup.find_all(True):
        if tag.name in tags:
            tag.unwrap()


def merge_tag_contents(soup):
    """
    :type soup: bs4.BeautifulSoup
    """
    for tag in soup.find_all(True):
        merged_contents = _merge_content_list(tag.contents)
        tag.contents = merged_contents


def _merge_content_list(contents):
    def append_new_content(new_contents, next_content):
        if not new_contents:
            return [next_content]
        elif isinstance(next_content, NavigableString) and isinstance(
                new_contents[-1], NavigableString):
            new_contents[-1] = new_contents[-1].join([" ", next_content])
        else:
            new_contents.append(next_content)
        return new_contents
    return reduce(append_new_content, contents, [])