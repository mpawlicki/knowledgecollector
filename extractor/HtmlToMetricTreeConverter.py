from bs4 import NavigableString

__author__ = 'michal'
from extractor.MetricTreeLeaf import MetricTreeLeaf as Leaf
from extractor.MetricTreeNode import MetricTreeNode as Node
from extractor.BeautifulSoupUtils import *


class HtmlToMetricTreeConverter(object):
    def __init__(self, energy_dispersion=1):
        self.energy_dispersion = energy_dispersion

    def convert(self, soup, metric):
        """
        Creates tree with nodes containing computed metric
        basing on Beautiful Soup tree object and given metric
        """
        tree_depth = find_tree_depth(soup)
        self.__comp_cnt = 0
        self.__sum_cnt = 0
        return self.__convert(soup, metric, tree_depth)

    def __convert(self, soup, metric, tree_depth):
        return self.__make_node(soup, metric, tree_depth) \
            if is_node(soup) \
            else self.__make_leaf(soup, metric)

    def __make_node(self, soup, metric, tree_depth):
        node = Node()

        for child_soup in soup.children:
            converted_child = self.__convert(child_soup, metric, tree_depth)
            node.add_child(converted_child)

        node.energy = self.__compute_node_energy(node, tree_depth)

        return node

    def __compute_node_energy(self, node, tree_depth):
        self.__sum_cnt += 1
        #print "Sum!", str(self.__sum_cnt)
        child_nodes_energy = reduce(lambda x, y: x + y.energy, node.children, 0.0)
        return float(self.energy_dispersion) / tree_depth * child_nodes_energy

    def __make_leaf(self, soup, metric):
        text = ""
        if isinstance(soup, NavigableString):
            text = unicode(soup)
        self.__comp_cnt += 1
        #print "Computing!", str(self.__comp_cnt), text
        energy = metric.compute(text)

        leaf = Leaf(text, energy)

        return leaf
