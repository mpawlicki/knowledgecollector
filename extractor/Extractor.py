from bs4 import BeautifulSoup
from extractor.BeautifulSoupUtils import strip_tags

from extractor.CoherentNodeFinder import find_coherent_nodes
from extractor.CoherentNodeToObjectConverter import CoherentNodeToObjectConverter
from extractor.HtmlToMetricTreeConverter import HtmlToMetricTreeConverter
from extractor.preprocessing import replace_twos_in_superscripts, replace_nbsps, \
    clean_white_characters


__author__ = 'michal'


class Extractor(object):
    def __init__(self, metric):
        self.metric = metric

    def extract(self, html_string):
        html_string = replace_twos_in_superscripts(replace_nbsps(html_string))
        soup = BeautifulSoup(html_string)
        strip_tags(soup, ["a", "b", "i", "span", "strong", "sup", "u"])
        soup = BeautifulSoup(clean_white_characters(soup.prettify()))
        metric = self.metric
        metric_tree_root = HtmlToMetricTreeConverter(20.0).convert(soup.body,
                                                                   metric)
        coherent_nodes = find_coherent_nodes(metric_tree_root)
        return CoherentNodeToObjectConverter().convert(coherent_nodes)