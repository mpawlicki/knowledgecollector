from math import exp

from extractor.MetricTreeLeaf import MetricTreeLeaf

from utils.Collections import ThreadUnsafeQueue


__author__ = 'michal'


def __all_children_are_weaker(metric_tree_node):
    children = metric_tree_node.children
    children_with_higher_energy = filter(lambda child: child.energy > metric_tree_node.energy, children)
    return len(children_with_higher_energy) == 0


def __node_to_heap_priority(node):
    return exp(-node.energy)


def __compare_energies(x, y):
    return (x == y and 0) or (y < x and -1) or 1


def find_coherent_nodes(metric_tree_root):
    node_queue = ThreadUnsafeQueue()
    found_nodes = []
    node_queue.add(metric_tree_root)
    while not node_queue.is_empty():
        node = node_queue.remove()
        if (type(node) is MetricTreeLeaf) or __all_children_are_weaker(node):
            found_nodes.append(node)
        else:
            for child in node.children:
                node_queue.add(child)
    return sorted(found_nodes, __compare_energies, lambda tree_node: tree_node.energy)