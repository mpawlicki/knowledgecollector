__author__ = 'michal'


class Metric(object):
    """
    A class which constitutes an interface for the classes
    that compute a metric for a given piece of text
    """

    def compute(self, text):
        """
        :param text: the text for which the metric is to be computed
        :rtype : float
        """
        raise NotImplementedError