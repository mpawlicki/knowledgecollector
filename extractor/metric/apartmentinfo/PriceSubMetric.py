# coding=UTF-8
from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'

__currency = ur"(zł|zl|pln)"
__space = ur"(\s+)"
__price_value = ur"((\d{2,}(" + __space + ur"|\.)+)\d{3,}([,.]\d{2})?)"

price_regex_to_value_dict = {
    ur"cena": 800.0,
    __currency + __space + __price_value: 700.0,
    __price_value + __space + __currency: 700.0,
    ur"cena:?" + __space + __price_value + __space + __currency: 1500.0,
    ur"cena:?" + __space + __price_value: 1000.0
}


class PriceSubMetric(GenericSubMetric):
    def __init__(self):
        super(PriceSubMetric, self).__init__(price_regex_to_value_dict)