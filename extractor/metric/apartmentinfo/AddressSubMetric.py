# coding=UTF-8

from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'

__address_word = ur"(adres:?|lokalizacja:?)"
__pre_word = ur"(ul\.|ulica|al\.|aleja|os\.|osiedle)"
__name = ur"(\w+(,? \w+)*)"
__number = ur"(\d{1,3}(( *m\. *| */ *)\d{1,3}))"

address_regex_to_value_dict = {
    __address_word: 800.0,
    __pre_word: 200.0,
    ur"{0} *{1}".format(__pre_word, __name): 800.0,
    ur"{0} *{1} *{2}".format(__pre_word, __name, __number): 900.0,
    ur"{0} *{1}* *{2} *{3}".format(__address_word, __pre_word, __name,
                                   __number): 1500.0,
    ur"{0} *{1}".format(__name, __number): 600.0,
    ur"{0} *{1}* *{2}".format(__address_word, __pre_word, __name): 1200.0
}


class AddressSubMetric(GenericSubMetric):
    def __init__(self):
        super(AddressSubMetric, self).__init__(address_regex_to_value_dict)