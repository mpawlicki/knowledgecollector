# coding=UTF-8

from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'

__surface_keyword = ur"(powierzchnia|wielko(ść|sc)|metra[zż])"
__square_meters = ur"(m2|m\.? *kw\.?)"
__surface_value = ur"(\d{2,4}([,.]\d+)?)"

surface_regex_to_value_dict = {
    __square_meters: 500.0,
    __surface_keyword + ur":?": 800.0,
    __surface_keyword + ur" *\(? *" + __square_meters + ur" *\)?": 900.0,
    __surface_keyword + ur" *\(? *" + __square_meters + ur" *\)?:?" + ur" *" +
    __surface_value: 1400.0,
    __surface_keyword + ur":? *" + __surface_value: 1200.0,
    __surface_keyword + ur":? *" + __surface_value + ur" *\(? *" +
    __square_meters + ur" *\)?": 1400.0,
    __surface_value + ur" *\(? *" + __square_meters + ur" *\)?": 600.0
}


class SurfaceSubMetric(GenericSubMetric):
    def __init__(self):
        super(SurfaceSubMetric, self).__init__(surface_regex_to_value_dict)