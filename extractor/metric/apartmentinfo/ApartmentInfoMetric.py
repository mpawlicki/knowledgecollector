# coding=UTF-8
from extractor.metric.apartmentinfo.AddressSubMetric import AddressSubMetric
from extractor.metric.apartmentinfo.PriceSubMetric import PriceSubMetric
from extractor.metric.apartmentinfo.SurfaceSubMetric import SurfaceSubMetric
from extractor.metric.apartmentinfo.emailsubmetric import EmailSubMetric
from extractor.metric.apartmentinfo.numberofroomssubmetric import \
    NumberOfRoomsSubMetric
from extractor.metric.apartmentinfo.phonenumbersubmetric import \
    PhoneNumberSubMetric
from extractor.metric.apartmentinfo.submissiondate import \
    SubmissionDateSubMetric

from extractor.metric.util.GenericMetric import GenericMetric

__author__ = 'michal'


class ApartmentInfoMetric(GenericMetric):
    def __init__(self):
        sub_metrics = [
            PriceSubMetric(),
            SurfaceSubMetric(),
            AddressSubMetric(),
            PhoneNumberSubMetric(),
            EmailSubMetric(),
            NumberOfRoomsSubMetric(),
            SubmissionDateSubMetric()
        ]
        super(ApartmentInfoMetric, self).__init__(sub_metrics)