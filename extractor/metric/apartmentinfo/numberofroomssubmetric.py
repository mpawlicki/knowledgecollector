# coding=UTF-8
from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'

__separator = ur"(\s+|-)"
__number_keyword = ur"(liczba pokoi|pokoje)"
__room_keyword = ur"(pok[oó]j|pokoje|pokoi|pokojowe)"
__number_value = ur"(\d{1,2})"

_number_of_rooms_regex_to_value_dict = {
    __number_keyword: 800.0,
    __number_keyword + ur"(:?)" + __separator + ur"?" + __number_value: 1500.0,
    __number_value + __separator + ur"?" + __room_keyword: 1400.0
}


class NumberOfRoomsSubMetric(GenericSubMetric):
    def __init__(self):
        super(NumberOfRoomsSubMetric, self).__init__(
            _number_of_rooms_regex_to_value_dict)