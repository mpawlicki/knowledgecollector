# coding=utf-8
from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'


__keyword = ur"(data dodania:?|dodano:?|zamieszczono:?)"
__space = ur"(\s+)"
__day = ur"([0-2]?[0-9]|3[01])"
__month = ur"(0?[0-9]|1[0-2])"
__year = ur"((20)?[0-9]{2})"
__separator = ur"(\.|/|-)"
__date_regex_1 = ur"({0}(?P<sep>{1}){2}(?P=sep){3})".format(
    __day, __separator, __month, __year
)
__date_regex_2 = ur"({0}(?P<sep>{1}){2}(?P=sep){3})".format(
    __year, __separator, __month, __day
)

_date_regex_to_value_dict = {
    __keyword: 800.0,
    __keyword + __space + __date_regex_1: 1500.0,
    __keyword + __space + __date_regex_2: 1500.0,
    __date_regex_1: 600.0,
    __date_regex_2: 600.0
}


class SubmissionDateSubMetric(GenericSubMetric):
    def __init__(self):
        super(SubmissionDateSubMetric, self).__init__(_date_regex_to_value_dict)