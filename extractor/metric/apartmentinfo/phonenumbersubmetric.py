# coding=UTF-8
from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'

__phone_number_keyword = ur"(telefon|tel\.?|kontakt|n(ume)?r tel(efonu|\.)?)"
__separator = ur"(\s+|-)"
__phone_number_value_1 = ur"((\+48{0}*)?\d{{3}}{0}\d{{3}}{0}\d{{3}})".format(
    __separator)
__phone_number_value_2 = ur"((\+48{0}*)?\d{{2}}{0}\d{{2,3}}{0}\d{{2}}{0}" \
                         ur"\d{{2,3}})".format(__separator)

_phone_number_regex_to_value_dict = {
    __phone_number_keyword: 800.0,
    __phone_number_value_1: 600.0,
    __phone_number_value_2: 600.0,
    __phone_number_keyword + ur" *" + __phone_number_value_1: 1500.0,
    __phone_number_keyword + ur" *" + __phone_number_value_2: 1500.0
}


class PhoneNumberSubMetric(GenericSubMetric):
    def __init__(self):
        super(PhoneNumberSubMetric, self).__init__(
            _phone_number_regex_to_value_dict)