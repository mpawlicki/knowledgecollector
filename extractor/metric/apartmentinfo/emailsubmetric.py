# coding=UTF-8
from extractor.metric.util.GenericSubMetric import GenericSubMetric

__author__ = 'michal'


__email_keyword = ur"((adres +)?e-?mail)"
__email_value = ur"([\w\.]+@(\w+\.)+\w+)"

_email_regex_to_value_dict = {
    __email_keyword: 800.0,
    __email_value: 1200.0,
    __email_keyword + ur":? *" + __email_value: 1500.0
}


class EmailSubMetric(GenericSubMetric):
    def __init__(self):
        super(EmailSubMetric, self).__init__(_email_regex_to_value_dict)