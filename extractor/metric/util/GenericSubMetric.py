import re

from extractor.metric.api.Metric import Metric


__author__ = 'michal'


class GenericSubMetric(Metric):
    """
    A utility class for creating 'sub-metrics'. Such a 'sub-metric'
    scans the text in search of several regular expressions. Each expression
    represents a piece of information and is considered more or
    less valuable (important). The final value of the metric is computed
    as the maximal value of the 'importance factors' of the expressions
    that have been found in the text.
    """

    def __init__(self, regex_to_value_dict):
        """
        :param regex_to_value_dict: a dictionary which maps regular
        expression to their 'importance factors'.
        """
        assert type(regex_to_value_dict) is dict
        self.__pattern_to_value_dict = {}
        for regex in regex_to_value_dict:
            pattern = re.compile(regex, re.IGNORECASE | re.DOTALL | re.MULTILINE | re.UNICODE)
            self.__pattern_to_value_dict[pattern] = regex_to_value_dict[regex]

    def compute(self, text):
        assert isinstance(text, basestring)
        matching_patterns = filter(lambda pattern: pattern.search(text) is not None, self.__pattern_to_value_dict)
        return max(map(lambda pattern: self.__pattern_to_value_dict[pattern], matching_patterns)) if len(
            matching_patterns) > 0 else 0.0