from extractor.metric.api.Metric import Metric

__author__ = 'michal'


class GenericMetric(Metric):
    """
    A utility class for creating metrics whose values evaluate
    to the sum of their 'sub-metrics'. It means that the piece of text
    for which the metric is computed is passed to the sub-metrics, and
    the final value of the metric is the sum of the values returned
    by the 'sub-metrics'.
    """

    def __init__(self, sub_metrics):
        self.__sub_metrics = sub_metrics

    def compute(self, text):
        assert isinstance(text, basestring)
        metric_elements = map(lambda sub_metric: sub_metric.compute(text), self.__sub_metrics)
        almost_ready_metric = reduce(lambda x, y: x + y, metric_elements, 0.0)
        return self.__apply_info_density_factor(almost_ready_metric, text)

    @staticmethod
    def __apply_info_density_factor(almost_ready_metric, text):
        return almost_ready_metric / max(1.0, len(text) - 40)




