# coding=UTF-8

from extractor.metric.apartmentinfo.ApartmentInfoMetric import ApartmentInfoMetric

__author__ = 'michal'

import unittest


class ApartmentInfoMetricTests(unittest.TestCase):
    def setUp(self):
        self.instance = ApartmentInfoMetric()

    def test_something(self):
        print self.instance.compute("cena")
        print self.instance.compute("Cena:")
        print self.instance.compute("Lubię placki:")
        print self.instance.compute("235 324,43 zł")
        print self.instance.compute("pln235 324 43")
        print self.instance.compute("Cena: 235 324.43 pln")
        print self.instance.compute("Zł  799 000,00")
        print self.instance.compute("Zł 799 000,00")


if __name__ == '__main__':
    unittest.main()
