from extractor.CoherentNodeFinder import find_coherent_nodes
from extractor.MetricTreeLeaf import MetricTreeLeaf
from extractor.MetricTreeNode import MetricTreeNode

__author__ = 'michal'

import unittest


class CoherentNodeFinderTest(unittest.TestCase):
    def test_should_coherent_node_finder_find_appropriate_nodes(self):
        node_0 = MetricTreeNode(5.0)
        node_0_0 = MetricTreeNode(10.0)
        node_0.add_child(node_0_0)
        node_0_0_0 = MetricTreeLeaf("", 6.0)
        node_0_0.add_child(node_0_0_0)
        node_0_0_1 = MetricTreeLeaf("", 4.0)
        node_0_0.add_child(node_0_0_1)
        node_0_1 = MetricTreeNode(3.0)
        node_0.add_child(node_0_1)
        node_0_1_0 = MetricTreeLeaf("", 1.0)
        node_0_1.add_child(node_0_1_0)
        node_0_1_1 = MetricTreeLeaf("", 4.0)
        node_0_1.add_child(node_0_1_1)
        coherent_nodes = find_coherent_nodes(node_0)
        self.assertListEqual(coherent_nodes, [node_0_0, node_0_1_1, node_0_1_0])


if __name__ == '__main__':
    unittest.main()
