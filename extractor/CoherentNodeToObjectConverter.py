# coding=utf-8
import re

from extractor.MetricTreeLeaf import MetricTreeLeaf
from extractor.apartmentinfo import has_complete_apartment_info, ADDRESS_KEY, \
    SURFACE_KEY, PRICE_KEY, PHONE_NUMBER_KEY, EMAIL_KEY, NUMBER_OF_ROOMS_KEY, \
    SUBMISSION_DATE_KEY
from extractor.picking.addresspicking import AddressPicker
from extractor.picking.emailpicking import EmailPicker
from extractor.picking.numberofroomspicking import NumberOfRoomsPicker
from extractor.picking.phonenumberpicking import PhoneNumberPicker
from extractor.picking.pricepicking import PricePicker
from extractor.picking.submissiondate import SubmissionDatePicker
from extractor.picking.surfacepicking import SurfacePicker
from extractor.normailzation.NormalizationError import NormalizationError
from extractor.normailzation.apartmentinfo.AddressNormalizer import AddressNormalizer
from extractor.normailzation.apartmentinfo.EmailNormalizer import EmailNormalizer
from extractor.normailzation.apartmentinfo.NumberOfRoomsNormalizer import NumberOfRoomsNormalizer
from extractor.normailzation.apartmentinfo.PhoneNumberNormalizer import PhoneNumberNormalizer
from extractor.normailzation.apartmentinfo.PriceNormalizer import PriceNormalizer
from extractor.normailzation.apartmentinfo.SurfaceNormalizer import SurfaceNormalizer
from extractor.normailzation.apartmentinfo.SubmissionDateNormalizer import SubmissionDateNormalizer

__author__ = 'michal'

re_flags = re.DOTALL | re.MULTILINE | re.UNICODE | re.IGNORECASE
whitespace_pattern = re.compile(r"^\s+$", re_flags)


class CoherentNodeToObjectConverter(object):
    def __init__(self):
        super(CoherentNodeToObjectConverter, self).__init__()

        self._property_info_suppliers = [
            ('price', PricePicker(), PriceNormalizer()),
            ('address', AddressPicker(), AddressNormalizer()),
            ('surface', SurfacePicker(), SurfaceNormalizer()),
            ('phone_number', PhoneNumberPicker(), PhoneNumberNormalizer()),
            ('email', EmailPicker(), EmailNormalizer()),
            ('number_of_rooms', NumberOfRoomsPicker(), NumberOfRoomsNormalizer()),
            ('submission_date', SubmissionDatePicker(), SubmissionDateNormalizer()),
        ]

    def convert(self, coherent_nodes):
        apartment_info_dict = {}
        for coherent_node in coherent_nodes:
            for leaf in self.__leaves_in_metric_order(coherent_node):
                self.__supply_needed_info(apartment_info_dict, leaf)
                if has_complete_apartment_info(apartment_info_dict):
                    return apartment_info_dict
        return apartment_info_dict

    def __leaves_in_metric_order(self, node):
        if isinstance(node, MetricTreeLeaf):
            yield node
            raise StopIteration
        for sub_node in sorted(node.children, key=lambda child: child.energy,
                               reverse=True):
            for leaf in self.__leaves_in_metric_order(sub_node):
                yield leaf
        raise StopIteration

    def __supply_needed_info(self, apartment_info_dict, leaf):
        """
        :type apartment_info_dict: dict
        """
        text = unicode(leaf.text)
        # print "Text:", text
        # print "Energy:", leaf.energy
        # print apartment_info
        # print ""
        if whitespace_pattern.match(text):
            return

        def supply_property_info(apartment, attr_name, picker, normalizer):
            if attr_name not in apartment:
                attr_value = picker.try_to_extract_info(text)
                try:
                    if attr_value:
                        attr_value = normalizer.normalize(attr_value)
                except NormalizationError:
                    attr_value = None

                if attr_value:
                    apartment[attr_name] = attr_value

        map(lambda x: supply_property_info(apartment_info_dict, x[0], x[1],
                                           x[2]), self._property_info_suppliers)
