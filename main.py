import sys

from persistence.DataStore import DataStore
from extractor.metric.apartmentinfo.ApartmentInfoMetric import ApartmentInfoMetric as Metric
from crawler.crawl import WebsiteDownloader as Downloader
from extractor.Extractor import Extractor

__author__ = 'michal'


def main(url_address):
    downloader = Downloader()
    metric = Metric()
    extractor = Extractor(metric)
    dataStore = DataStore()

    # TODO: Catch exceptions of WebsiteDownloader
    html = downloader.download(url_address)
    results = extractor.extract(html)

    # TODO: Print results with more details
    for result in results:
        print result

    dataStore.save_all(results)


if __name__ == "__main__":
    try:
        url = sys.argv[1]
    except IndexError:
        sys.stderr.write("Usage: python main http://www.website.url")
        sys.exit(-1)
    else:
        main(url)
